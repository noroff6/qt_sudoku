#include <QCoreApplication>
#include <iostream>
#include <vector>
#include <algorithm>

int grid[3][3] = {{2, 1, 4}, {3, 9, 7}, {5, 6, 8}};

void checkSoduku(std::vector<int> &sorted_numbers)
{


    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            sorted_numbers.push_back(grid[i][j]);
        }
    }
    sort(sorted_numbers.begin(), sorted_numbers.end());
    int count = 1;
    for (int i : sorted_numbers)
    {
        for (int j = 1; j < 10; j++)
        {
            if (i == count)
            {
                count++;
            }
        }

        if (count == 9)
        {
            std::cout << "Numbers 1-9 are uniquely present in the sudoku. " << std::endl;
        }
    }

    if (count < 9)
    {
        std::cout << "Numbers 1-9 are NOT uniquely present in the sudoku. " << std::endl;
    }
}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    std::vector<int> sorted_numbers;
    checkSoduku(sorted_numbers);

    return a.exec();
}
